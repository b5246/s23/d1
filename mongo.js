//VIA CLI
// Select a database 
use <database name>
//================== CRUD

//====================Insert One doc (create)
db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "python"],
	department: "None"

})

//Insert many
db.users.insert([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@gmail.com"
		},
		courses: ["PHP", "React", "python"],
		department: "None"
	},
	{
		firstName: "John",
		lastName: "Smith",
		age: 82,
		contact: {
			phone: "123456789",
			email: "johnsmith@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "None"
	}
])

//==========================READ
db.users.find()
//copy and paste to shell of course booking database
// retrieve a list of ALL users

//specific User
db.users.find({firstName: "Stephen"});

//unique-occurance
db.users.findOne({firstName: "Stephen"});

//find documents with multiple paramters/conditions
db.users.find({lastName: "Smith", age: 82})

//===========================UPDATE
//SAMPLE INSERT
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "123456789",
		email: "test@gmail.com"
	},
	courses: [],
	department: "None"
})

//Needs two object
//object1: specify which document to update
//object2: Specific feilds to update
db.users.updateOne(
	{ // FOR TEST USERNAME
		_id: ObjectId("62876c4865be0f5fad9a8d05")
	},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Opearations"
		}
	}
)

//TO EDIT goto ROBO3T>wrong entry
//goto tree-mode and manually edit.

//UPDATE MULTIPLE DOCUMENTS
//NOTE: entries case sensitive
db.users.updateMany(
	{
		department: "None"
	},
	{
		$set: {
			department: "HR"
		}
	}
)

//Deleting a single document
db.users.insert(
	{
		firstName: "test"
	}
)

db.users.deleteOne(
	{
		firstName: "test"
	}
)

// Deleting many documents
//Delete cautiously
db.posts.deleteMany()

//deletion 2 entries
//A)
db.users.insert({
	firstName: "Bill"
})
//B)
db.users.deleteMany({
	firstName: "Bill"
})
























